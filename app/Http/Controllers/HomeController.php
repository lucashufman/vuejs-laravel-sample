<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Input;
use Auth;

class HomeController extends Controller
{
    public function __construct() {
    }
    
    public function __invoke(Request $request)
    {
        //
    }

    public function index(){
        return view('welcome', array( "intended" => Redirect::intended()->getTargetUrl() ));
    }

    public function email($view) {
        return view('emails.'. $view);
    }
}
