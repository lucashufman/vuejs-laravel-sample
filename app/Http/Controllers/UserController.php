<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\ContractService;
use App\Models\Login;
use App\Models\ZoopToken;
use Zoop\Facades\ZoopBuyers;
use App\Jobs\SendNewUserGreetings;
use App\Jobs\SendPasswordReset;
use App\Helpers\GlobalHelper;

use Hashids;
use Hash;
use Input;
use Validator;
use Auth;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    private $userService, $contractService;
    public function __construct(){
        $this->userService     = new UserService();
        $this->contractService = new ContractService();
    }

    public function listing(){
        $users = $this->userService->getList();
        $users = $users->filter(function($value) {
            $value->id = Hashids::encode($value->id);
            return $value;
        });
        return $users;
    }

    public function createHandler(){
        $data              = Input::all();
        $data['password']  = Hash::make($data['password']);
        $data['navigator'] = json_encode($data['navigator']);
        $rules             = array( 'email' => 'unique:users,email' );
        $validator         = Validator::make($data, $rules);
        
        if ($validator->fails()) {
            $result = array(
                'success' => false,
                'message' => 'Esse endereço de e-mail já foi registrado. Tem certeza de que você não tem uma conta?'
            );
        } else {
            $result = array( 'success' => true );
            try {
                $user   = $this->userService->create(array(
                    "name"     => $data['name'],
                    "email"    => $data['email'],
                    "password" => $data['password']
                ));
                Login::create(array(
                    "user_id"     => $user->id,
                    "hash"        => md5(uniqid()),
                    "navigator"   => $data['navigator'],
                    "external_ip" => \Request::ip(),
                    "internal_ip" => ""
                ));
                Auth::login($user);
                
                $fullname  = explode(" ", $data['name'], 2);
                $supername = empty($fullname[1]) ? '' : $fullname[1];
                $buyer     = ZoopBuyers::create([
                    'first_name'  => $fullname[0],
                    'last_name'   => $supername,
                    'description' => 'Comprador - Sending',
                    'email'       => $data['email'],
                ]);
                $this->userService->update(array(
                    "id"         => $user->id,
                    "zoop_token" => $buyer->id
                ));

                // dispacha o job onde vai enviar as o email de agradecimento ao usuário
                $newUserGreetingsEmail = (new SendNewUserGreetings($user))->onQueue('default');
                dispatch($newUserGreetingsEmail);
            } catch (\Exception $e) {
                return response()->json(array(
                    'success' => false,
                    'message' => $e->getMessage(),
                    'trace'   => $e->getTrace(),
                ));
            }
        }

        return $result;
    }

    public function loginHandler(){
        $data    = Input::all();
        $attempt = Auth::attempt(array( "email" => $data['email'], "password" => $data['password'] ));
        if ($attempt) {
            return array( "success" => true );
        } else {
            return array(
                "success" => false,
                "message" => "O endereço de email ou a senha que você inseriu não é válido. Tente novamente."
            );
        }
    }

    public function logoutHandler(){
        Auth::logout();
        return array( "success" => true );
    }

    public function cards(Request $request){
        $user  = $request->user();
        $cards = ZoopToken::where('user_id', $user->id)->select('id', 'name')->get()->toArray();
        $cards = GlobalHelper::arrayEncodeId($cards);

        return response()->json(array(
            'success' => true,
            'data'    => $cards
        ));
    }

    public function balance($id, Request $request) {
        try {
            $totalBalance = $this->contractService->getTotalBalanceByUser($id);

            return response()->json(array(
                'success' => true,
                'data' => array(
                    'smsBalance'   => $totalBalance['sms'],
                    'emailBalance' => $totalBalance['email']
                )
            ));
        } catch (\Exception $e) {
            return response()->json(array(
                'success' => true,
                'data'    => array(
                    'smsBalance'   => 0,
                    'emailBalance' => 0
                ),
                'message' => $e->getMessage(),
            ));
        }
    }

    public function resetPassword(Request $request) {
        $email = $request->email;
        if (!empty($email)) {
            $user = $this->userService->getByFilters(array( 'email' => $email ))->first();
            if ($user) {
                $data = array(
                    'id' => $user->id,
                    'reset_password_token' => str_random(32)
                );

                if ($this->userService->update($data)) {
                    $mailJob = (new SendPasswordReset($user, $data['reset_password_token']))->onQueue('default');
                    dispatch($mailJob);

                    return response()->json(array(
                        'success' => true,
                        'message' => 'Foram enviadas as instruções de recuperação de senha para sua conta de e-mail.'
                    ));
                }
            } else {
                return response()->json(array(
                    'success' => false,
                    'message' => 'Não existe nenhuma conta vinculada a este e-mail'
                ));
            }
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'O campo de e-mail deve ser preenchido'
            ));
        }
    }

    public function recoveryPassword() {
        $data = Input::all();
        $rules = array(
            'confirmation_password' => 'required',
            'password' => 'required',
            'email'    => 'required',
            'token'    => 'required'
        );

        $validation = Validator::make($data, $rules);
        if ($validation->passes()) {
            if ($data['password'] == $data['confirmation_password']) {
                $user = $this->userService->getByFilters(array( 'email' => $data['email'], 'reset_password_token' => $data['token'] ))->first();

                if ($user) {
                    $data = array(
                        'id'       => $user->id,
                        'password' => bcrypt($data['password']),
                        'reset_password_token' => null
                    );

                    if ($this->userService->update($data)) {
                        return response()->json(array(
                            'success' => true,
                            'message' => 'Alteração realizada com sucesso! Redirecionando...'
                        ));
                    } else {
                        return response()->json(array(
                            'success' => false,
                            'message' => ''
                        ));
                    }
                } else {
                    return response()->json(array(
                        'success' => false,
                        'message' => 'Ocorreu um erro ao tentar realizar alteração de senha'
                    ));
                }
            } else {
                return response()->json(array(
                    'success' => false,
                    'message' => 'As senhas não coincidem'
                ));
            }
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Preencha todos os campos, por favor'
            ));
        }
    }

    public function verifyRecovery() {
        $data  = Input::all();
        $rules = array( 'email' => 'required', 'recovery_token' => 'required' );

        $validation = Validator::make($data, $rules);
        if ($validation->passes()) {
            $user = $this->userService->getByFilters(array( 'email' => $data['email'], 'reset_password_token' => $data['recovery_token'] ))->first();

            if ($user) {
                return response()->json(array( 'success' => true ));
            } else {
                return response()->json(array(
                    'success' => false,
                    'message' => 'E-mail e token não coincidem com nenhum usuário'
                ));
            }
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Campos faltando.'
            ));
        }
    }
}