<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ContractService;
use App\Helpers\GlobalHelper;
use Auth;

class ContractController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    private $contractService;
    public function __construct(){
        $this->contractService = new ContractService();
    }

    public function listing(Request $request){
		$user = $request->user();
		
		// SUA LÓGICA AQUI
		$contracts = $this->contractService->getList();
        // $contracts = GlobalHelper::collectionEncodeId($contracts);

        return response()->json(array(
            'status' => true,
            'data'    => array(
				'contracts' => $contractsd,
				'ato' => $ato
			),
			'message' => 'Sucesso!'
        ));
    }
}
