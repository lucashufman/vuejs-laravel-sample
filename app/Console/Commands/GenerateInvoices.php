<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
// use App\Services\InvoiceService;
// use App\Services\ContractUserService;
use App\Jobs\CreateInvoice;

class GenerateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sending:generate-invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gerar faturas do mês anterior';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $response;
    private $invoiceService;
    // private $contractUserService;
    public function __construct()
    {
        parent::__construct();
        // $this->invoiceService      = new InvoiceService();
        // $this->contractUserService = new ContractUserService();
    }

    /**
     * Execute the console command.  
     *
     * @return mixed
     */
    public function handle()
    {
        // $contractUsers = $this->contractUserService->getRecurringContractsList();
        // foreach($contractUsers as $contractUser){
        //     CreateInvoice::dispatch($contractUser)->delay(now()->addSeconds(5)); 
        // }
    }
}
