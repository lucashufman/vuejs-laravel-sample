<?php

namespace App\Http\Controllers\Api\v1;

use Input;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    public function link(Request $request){
        $user = $request->user();
        $data = $request->all();
        $platformsAllowed = ['wispot'];
        if(in_array($data['paltform'], $platformsAllowed))
        {
            $field = $data['paltform']."_token";
            $value = $data['token'];
            User::where('id', $user->id)->update([$field=>$value]);    
            return response()->json([
				'success' => true,
				'message' => 'Account binding performed successfully!'
			], 200);
        }else{
            return response()->json([
				'success' => false,
				'message' => 'Unauthorized linking.'
			], 200);
        }           
    }
}