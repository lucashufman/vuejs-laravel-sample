@extends('email-layout')
@section('title', ($isPlan ? 'Plano contratado' : 'Pacote contratado'))
@section('content')
    <style>
        body { margin: 0; padding: 0; }
        table { color: #ACACAC; font-family: sans-serif; font-size: 14px; line-height: 140%; }
        .container-fluid { background-color: #EEE; }
        .mini-divider { height: 25px; }
        .divider { height:30px; }
        .divider-pipe { height: 1px; background-color: #ACACAC; }
        .table-container {
            background-color: #FFF;
            border-radius: 10px;
            overflow: hidden;
        }
        img.logo { width: 110px; margin: 0 auto; }
        .footer { font-size: 12px; }
    </style>
    <table class="container-fluid" cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
        <tbody>
            <tr><td valign="top" class="divider">&nbsp;</td></tr>
            <tr>
                <td valign="top" align="center">
                    <table cellpadding="0" class="table-container" width="550" cellspacing="0" align="center" border="0">
                        <tbody>
                            <tr><td valign="top" class="divider">&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <img class="logo" src="{{ Hp::resolvePanelHost() .'/img/emails/sending-logo.png' }}" alt="Sending" />
                                </td>
                            </tr>
                            <tr><td valign="top" class="divider">&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    @if ($isPlan)
                                        <img src="{{ Hp::resolvePanelHost() .'/img/emails/contracted-plan.jpg' }}" alt="Plano contratado" />
                                    @else
                                        <img src="{{ Hp::resolvePanelHost() .'/img/emails/contracted-pack.jpg' }}" alt="Pacote contratado" />
                                    @endif
                                </td>
                            </tr>
                            <tr><td valign="top" class="divider">&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center" width="385">
                                        <tr>
                                            <td valign="top" align="center">
                                                <p><strong>Olá, {{ $user->name }}!</strong></p>
                                                <p>Recebemos uma solicitação de contratação de um {{ $isPlan ? 'plano' : 'pacote' }} em nosso sistema.</p>
                                                <h3 style="font-size:18px;font-weight:700">Detalhes do plano</h3>
                                            </td>
                                        </tr>
                                        <tr><td valign="top" class="mini-divider"></td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center" width="460">
                                        <tr>
                                            <td valign="top" align="left">
                                                <p style="font-size:13px;color:#999999">
                                                    <strong>{{ $contract->name }}</strong><br />
                                                    <small>{{ $contract->description }}</small>
                                                </p>
                                            </td>
                                            <td valign="top" align="right">
                                                <p><strong>R$ {{ number_format(( $contract->amount / 100 ), 2, ',', '.') }}</strong></p>
                                            </td>
                                        </tr>
                                        <tr><td valign="top" class="divider" colspan="2"></td></tr>
                                        <tr><td valign="top" class="divider-pipe" colspan="2"></td></tr>
                                        <tr><td valign="top" class="divider" colspan="2"></td></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0" align="center" width="385">
                                        <tr>
                                            <td valign="top" align="center">
                                                <p>Se você não contratou nenhum {{ $isPlan ? 'plano' : 'pacote' }}, por favor desconsidere este e-mail.</p>
                                                <p>Pedimos desculpa pelo transtorno.</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td valign="top" class="divider">&nbsp;</td></tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr><td valign="top" class="divider">&nbsp;</td></tr>
            <tr>
                <td valign="top" align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="385" align="center" class="footer">
                        <tr>
                            <td valign="top" align="center">
                                <p>Wispot Marketing Connect LTDA - ME, Av. Imperatriz Leopoldina, 957, São Paulo - SP, 05305-011 - 20.522.147/0001-59</p>
                                <p>Powered by Sending</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td valign="top" class="divider">&nbsp;</td></tr>
        </tbody>
    </table>
@endsection