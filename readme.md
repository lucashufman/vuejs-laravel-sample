## Sobre o Projeto

Esta é uma aplicação pronta para programar utilizando o framework do VueJs com Vuex para Front-end e o Laravel para Back-end na mesma aplicação, sem necessidade de criar duas aplicações diferentes para cada um dos ambientes.

O sistema de autenticação utiliza o próprio passport do Laravel em conjunto com o Vuejs.

Documentação do Laravel: https://laravel.com/docs/5.7
Documentação do VueJs: https://vuejs.org/
Documentação do Vuex: https://vuex.vuejs.org/installation.html

## Introdução

Primeiramente tenha em sua máquina o Composer e o NodeJS instalado.
Após a instalação, execute os comandos abaixo no command line na pasta raiz de seu projeto:

> composer install

> npm install

Agora crie uma cópia do arquivo ".env.example" e renomeie para ".env".
Após isso, execute o seguinte comando:

> php artisan key:generate

Este comando irá criar a chave de criptografia de sua aplicação.
Para mais informações acesse este [link](https://laravel.com/docs/5.7)

## Configurações de Autenticação

Dentro do command line do windows ou linux, rode os comandos abaixo em sequência

> composer require laravel/passport

> php artisan migrate

> php artisan passport:install

> php artisan passport:keys

> php artisan passport:client

> php artisan passport:client --password

> php artisan passport:client --client

> php artisan passport:client --personal


## Instruções de Desenvolvimento

**Banco de dados**

Próximo passo é configurar o banco de dados. Para tal configuração, acesse [este link](https://laravel.com/docs/5.6/database#configuration)

**Codificando e visualizando**

Para ir testando as implementações a medida que elas forem sendo desenvolvidas, deixe rodando no command line o seguinte comando:

> npm run watch

Cada vez que você salvar os arquivos, o node fará uma varredura nos arquivos modificados e irá gerar o bundle para você só dar o refresh na página e ver o resultado.

**Final do código**

Caso queira fazer somente uma varredura no código para criar o bundle, utilize:

> npm run dev

## Instalando novos pacotes Javascript

Para adicionar novos pacotes de Javascript, utilize o comando:

> npm install [pacote] --save

## Instalando novos pacotes PHP

Para adicionar novos pacotes de PHP, utilize o composer:

> composer require "[pacote]"

## Deploy

Para realizar o deploy da aplicação, utilize o comando:

> npm run prod

