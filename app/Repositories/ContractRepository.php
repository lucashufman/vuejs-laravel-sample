<?php
namespace App\Repositories;

use App\Models\Contract;
use DB;

class ContractRepository{
    public function getList($where = NULL){
		$result = Contract::with('users');
		if(!is_null($where))
            $where($result);
        return $result;
    }
}