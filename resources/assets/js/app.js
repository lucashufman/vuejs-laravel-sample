require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import Icon from 'vue-awesome/components/Icon'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueTheMask from 'vue-the-mask'
import ToggleButton from 'vue-js-toggle-button'
import VuejsDialog from 'vuejs-dialog'
import Vuex from 'vuex'
import StoreData from './store'

import 'vue-awesome/icons'

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(Snotify, {
	toast: { position: SnotifyPosition.rightTop }
})
Vue.use(VueTheMask)
Vue.use(ToggleButton)
Vue.use(VuejsDialog)

Vue.component('icon', Icon)
import App from './components/App'
import { routes } from './router'

const store  = new Vuex.Store(StoreData)
const router = new VueRouter({
	routes,
	mode: 'history',
})

router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (!window.Laravel.authCheck) {
			next({
				path  : '/login',
				query : { redirect: to.fullPath }
			})
		} else {
			next()
		}
	}
	/*
	else if (to.matched.some(record => record.meta.requiresNonAuth)) {
		if (window.Laravel.authCheck) {
			next({ path: '/contracts' })
		}else{
			next()
		}
	}
	*/
	else {
		next() 
	}
})

const app = new Vue({
	el : '#app',
	components : { App },
	router,
	store,
})

