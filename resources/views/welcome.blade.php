<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf_token" content="{{ csrf_token() }}" />
        <title>Painel - Sending</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" type="image/png" href="/img/sending-icon-mini.png"/>
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>
        
        <script type="text/javascript">
            window.Laravel = {
                csrfToken : '{{ csrf_token() }}',
                appPath   : '{{ asset('') }}',
                intended  : '{{ $intended }}',
                authCheck : {{ empty(Auth::check()) ? "false" : "true" }},
                userId    : {{ empty(Auth::check()) ? "false" : Auth::user()->id }}
            }
            window.CKEDITOR_BASEPATH = window.Laravel.appPath +'node_modules/ckeditor/'

            @if(Auth::check())
                localStorage.setItem('user', '{"name":"{{ Auth::user()->name }}","email":"{{ Auth::user()->email }}"}')
            @endif
        </script>
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
