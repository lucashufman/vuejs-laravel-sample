<?php

namespace App\Services;

use App\Repositories\ContractRepository;
use App\Services\ContractUserService;

class ContractService {

	private $contractRepository, $contractUserService, $userService;
	public function __construct(){
        $this->contractRepository  = new ContractRepository();
        $this->contractUserService = new ContractUserService();
        $this->userService         = new UserService();
    }

    public function getById($id){
        $result = $this->contractRepository->getList(function($result) use($id){
            $result->where('id', $id);
        })->first();
        return $result;
    }

    public function getList(){
        $result = $this->contractRepository->getList()->get();
        return $result;
    }

    public function getByFilter($filters) {
        $result = $this->contractRepository->getList(function($query) use($filters) {
            $query->with('users');

            if (array_key_exists('user_id', $filters)) {
                $query->whereHas('users', function($q) use($filters) {
                    $q->where('user_id', '=', $filters['user_id']);
                });
            }

            if (array_key_exists('available', $filters)) {
                $query->whereHas('users', function($q) use($filters) {
                    $q->where('available', '>', 0);
                });
            }

            if (array_key_exists('status', $filters)) {
                $query->where('status', '=', $filters['status']);
            }
        })->get();

        return $result;
    }

    public function getTotalBalanceByUser($userId) {
        $user  = $this->userService->getAssociatedToCampaigns($userId);

        $smsBalance   = $this->contractUserService->calculateAvailableBalance($user[0]->contracts, 'SMS');
        $emailBalance = $this->contractUserService->calculateAvailableBalance($user[0]->contracts, 'EMAIL');

        return array(
            'sms'   => $smsBalance,
            'email' => $emailBalance
        );
    }
}