<?php
namespace App\Helpers;

use App;
use Hashids;

abstract class GlobalHelper {
    /*
     * @params
     * $items => Array de objetos
     * 
     * @return
     * Array de objetos com o id encondado com Hashids
    */
    public static function arrayEncodeId($items) {
        return array_map(function($item) {
            if (is_array($item)) {
                $item['id'] = Hashids::encode($item['id']);
            } else {
                $item->id = Hashids::encode($item->id);
            }

            return $item;
        }, $items);
    }

    public static function collectionEncodeId($collection) {
        return $collection->map(function($item) {
            $item->id = Hashids::encode($item->id);
            return $item;
        });
    }

    public static function objectEncodeId($item) {
        $item->id = Hashids::encode($item->id);
        return $item;
    }

    public static function hashids_decode($int) {
        $int = Hashids::decode($int);
        return end($int);
    }

    public static function getDateByAge($age, $end = false) {
        $format = 'Y-01-01';
        if (!$end) {
            $format = 'Y-12-31';
        }

        return date($format, strtotime('-'. $age .' years'));
    }

    public static function getIds($items) {
        $_itemsIds = array();
        foreach($items as $item) {
            $_itemsIds[] = $item->id;
        }

        return $_itemsIds;
    }

    public static function checkAndFormatDate($string, $dateFormat) {
        $formatString = false;

        if (preg_match('/^(\d{2})(?:\-|\/)(\d{2})(?:\-|\/)(\d{4})$/', $string)) {
            $string = preg_replace('/^(\d{2})(?:\-|\/)(\d{2})(?:\-|\/)(\d{4})$/', '$3-$2-$1', $string);
            $formatString = true;
        } else if (preg_match('/^(\d{4})(?:\-|\/)(\d{2})(?:\-|\/)(\d{2})$/', $string)) {
            $formatString = true;
        }

        if ($formatString)
            $string = date($dateFormat, strtotime($string));

        return $string;
    }

    public static function resolvePanelHost() {
        if (App::environment() == 'local') {
            return 'http://dev.sending.com.br';
        } else {
            return 'https://painel.sending.com.br';
        }
    }
}