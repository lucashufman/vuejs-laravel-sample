import axios from 'axios';
axios.defaults.headers.common = {
    'X-Requested-With' : 'XMLHttpRequest',
    'X-CSRF-TOKEN'     : window.Laravel.csrfToken
}

export default {
    state: {
        loading   : true
    },
    getters: {
        isLoading(state) {
            return state.loading
        }
    },
    mutations: {
		// atribuir e modificar variáveis de estado (state)
        NOT_LOADING: state => state.loading = false,
    },
    actions: {
		// chamadas no banco de dados
    }
}