<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\InfobipWispotSDK;
use App\Models\JobLog;

class SendPasswordReset implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user, $token;
    private $infobipWispotSDK;

    public $tries = 5;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $token)
    {
        $this->user  = $user;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $infobip = new InfobipWispotSDK('c5e2b6f5f1cc9d802b9ef749572f4af4-a03635e1-b33b-46c4-ab6d-9cf2cc52b670');

            $infobip->isEmail();
            $infobip->setFrom(array( 'name'  => 'Sending', 'email' => 'contato@sending.com.br' ));
            $infobip->setTo(array( $this->user->email ));
            $infobip->setSubject('Recuperação de senha');
            $infobip->setHTML(view('emails.reset-password', array( 'user' => $this->user ))->render());
            
            $responseMessage = $infobip->send();
            $statusCode      = 200;
        } catch (\Exception $e) {
            $responseMessage = $e->getMessage();
            $statusCode      = 500;
        }
        
        JobLog::create(array(
            'proccess_name'   => 'SendPasswordReset',
            'response_text'   => $responseMessage,
            'response_status' => $statusCode
        ));
    }

    public function retryUntil() {
        return now()->addSeconds(30);
    }
}
