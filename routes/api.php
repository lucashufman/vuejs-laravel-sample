<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

// rotas de reset de senha
Route::post('/users/reset-password', 'UserController@resetPassword');
Route::post('/users/recovery-password', 'UserController@recoveryPassword');
Route::post('/users/verify-recovery', 'UserController@verifyRecovery');

// Internal API
Route::group([ 'middleware' => 'auth:api' ], function() {
    // Route::get('/contracts', 'ContractController@listing');
});

// External API
Route::group([ 'middleware' => 'auth:api' ], function(){
    Route::group([ 'prefix' => 'v1' ], function () {
        Route::put('/users/link','Api\v1\UserController@link')->middleware('scopes:link-account');
    });
});
