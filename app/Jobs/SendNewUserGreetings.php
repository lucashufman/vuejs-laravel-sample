<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\InfobipWispotSDK;

class SendNewUserGreetings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user;

    public $tries = 5;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $infobip = new InfobipWispotSDK('c5e2b6f5f1cc9d802b9ef749572f4af4-a03635e1-b33b-46c4-ab6d-9cf2cc52b670');

        $infobip->isEmail();
        $infobip->setFrom(array( 'name'  => 'Sending', 'email' => 'contato@sending.com.br' ));
        $infobip->setTo(array( $this->user->email ));
        $infobip->setSubject('Bem-vindo ao Sending');
        $infobip->setHTML(view('emails.new-user-greetings', array( 'user' => $this->user ))->render());
        $infobip->send();
    }

    public function retryUntil() {
        return now()->addSeconds(30);
    }
}
