import AccountCreate from './components/account/Create'
import AccountLogin from './components/account/Login'
import ResetPassword from './components/account/ResetPassword'
import RecoveryPassword from './components/account/RecoveryPassword'

export const routes = [
    // { path: '/', name: 'Dashboard', component: Dashboard, meta : { requiresAuth : true } },
    { path: '/register', name: 'AccountCreate', component: AccountCreate, meta: { requiresNonAuth: true } },
    { path: '/login', name: 'AccountLogin', component: AccountLogin, meta: { requiresNonAuth: true } },
    { path: '/reset-password', name: 'ResetPassword', component: ResetPassword, meta: { requiresNonAuth: true } },
    { path: '/recovery-password', name: 'RecoveryPassword', component: RecoveryPassword, meta: { requiresNonAuth: true } },
    { path: '/introduction-guide', name: 'IntroductionGuide', component: IntroductionGuide, meta: { requiresNonAuth: true } },
]