<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/login", "HomeController@index")->name('login');
Route::post("/login", "UserController@loginHandler");
Route::get("/create", "HomeController@index");
Route::post("/register", "UserController@createHandler");
Route::post("/logout", "UserController@logoutHandler");

Route::get('/{any}', 'HomeController@index')->where('any', '.*');
